import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-register-form',
  templateUrl: './new-register-form.component.html',
  styleUrls: ['./new-register-form.component.css']
})
export class NewRegisterFormComponent implements OnInit {
  // لیست همه ی منوی آبشاری انتخابی
    categories=[
      {id:1, name:'Developers'},
      {id:2, name:'Designer'},
      {id:3, name:'manager'}
    ]
  constructor() { }
//  این که اطلاعات وارد شده در مرورگر رو بتونیم بگیریم و تو یه کنسول نشون بدیم
  submit(allInformation){
    console.log(allInformation);
  }

  ngOnInit(): void {
  }

}
